<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class ImportCSV extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'ImportCSV';
        $this->tab = 'others';
        $this->version = '1.0.0';
        $this->author = 'Yanis Corselle at Ecig & Zen';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Import CSV des Produits');
        $this->description = $this->l('Ce module permet d\'importer tous les produits d\'un site web au format CSV. Il peut être lancé par une tâche CRON.');

        $this->confirmUninstall = $this->l('');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('IMPORTCSV_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        Configuration::deleteByName('IMPORTCSV_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitImportCSVModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        $csv = "http://3s426adnqh.preview.infomaniak.website/modules/ExportCSV/export.csv";
        $fichier = fopen($csv, 'r');
        $i = 1;
        while (($data = fgetcsv($fichier, 1000, ",")) !== FALSE) {
            if($data[0] == "information_produit")
            {
                $reference = $data[1];
                if($data[7] == 0) {
                    $sql = 'SELECT COUNT(`reference`) FROM '._DB_PREFIX_.'product WHERE `reference` = ' . $reference;
                    if( Db::getInstance()->getValue($sql) == 0)
                        $produit_existe = 0;
                    elseif ( Db::getInstance()->getValue($sql) == 1)
                        $produit_existe = 1;
                    else
                        echo "Un bug s'est produit : Voir ligne ".$i;
                }

                elseif($data[7] == 1)
                {
                    $sql = 'SELECT COUNT(`reference`) FROM '._DB_PREFIX_.'product_attribute WHERE `reference` = ' . $reference;
                    if( Db::getInstance()->getValue($sql) == 0)
                        $produit_existe = 0;
                    elseif( Db::getInstance()->getValue($sql) == 1)
                        $produit_existe = 1;
                    else
                        echo "Un bug s'est produit : Voir ligne ".$i;
                }

                $titre = $data[2];
                $recapitulatif = $data[3];
                $description = $data[4];
                $marque = $data[5];
                $categorie = $data[6];
                $date = date("Y-m-d H:i:s");

                if(!empty($marque))
                {
                    $sql = 'SELECT COUNT(`name`) FROM '._DB_PREFIX_.'manufacturer  WHERE `name` = ' . $marque;
                    if( Db::getInstance()->getValue($sql) == 0)
                        $marque_existe = 0;
                    elseif( Db::getInstance()->getValue($sql) == 1)
                        $marque_existe = 1;
                    else
                        echo "Un bug s'est produit : Voir ligne ".$i;

                    if($marque_existe == 0)
                    {
                        Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."manufacturer` ('name', 'date_add', 'date_upd', 'active') VALUES(" . $marque. $date . $date . 1 . ")");
                    }
                }

                if(!empty($categorie))
                {
                    $id_categorie = Db::getInstance()->getValue('
                SELECT `id`
                FROM `'._DB_PREFIX_.'category_lang`
                WHERE `name` = '.$categorie);
                    $categorie_parent = $data[8];
                    if(!empty($categorie_parent))
                    {
                        $id_categorie_parent = Db::getInstance()->getValue('
                    SELECT `id`
                    FROM `'._DB_PREFIX_.'category_lang`
                    WHERE `name` = '.$categorie_parent);
                        $categorie_parent1 = $data[9];

                        if(!empty($categorie_parent1))
                        {
                            $id_categorie_parent1 = Db::getInstance()->getValue('
                        SELECT `id`
                        FROM `'._DB_PREFIX_.'category_lang`
                        WHERE `name` = '.$categorie_parent1);
                            $categorie_parent2 = $data[10];
                            if(!empty($categorie_parent2))
                            {
                                $id_categorie_parent2 = Db::getInstance()->getValue('
                            SELECT `id`
                            FROM `'._DB_PREFIX_.'category_lang`
                            WHERE `name` = '.$categorie_parent2);
                            }
                        }
                    }

                    if(!empty($categorie_parent2))
                    {
                        $sql = 'SELECT COUNT(`name`) FROM '._DB_PREFIX_.'category_lang  WHERE `name` = ' . $categorie_parent2;
                        if( Db::getInstance()->getValue($sql) == 0)
                            $categorie_existe = 0;
                        elseif( Db::getInstance()->getValue($sql) == 1)
                            $categorie_existe = 1;
                        else
                            echo "Un bug s'est produit : Voir ligne ".$i;

                        if($categorie_existe == 0)
                        {
                            $id_parent = 0;
                            $date = date("Y-m-d H:i:s");
                            Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."category` ('id_parent', 'date_add', 'date_upd') VALUES(" . $id_parent . $date . $date . ")");
                            Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."category_lang` ('id_lang', 'name', 'link_rewrite') VALUES(" . 2 . $categorie_parent2 . $categorie_parent2 . ")");
                        }
                    }


                }
                $id_marque = Db::getInstance()->getValue('
            SELECT `id`
            FROM `'._DB_PREFIX_.'manufacturer`
            WHERE `name` = '.$marque);


                if(($produit_existe == 0) && ($data[7] == 0))
                {
                    Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."product` ('reference', 'name', 'description_short', 'description', 'marque', 'id_category_default', 'date_add', 'date_upd') VALUES(". $reference . $titre. $recapitulatif . $description . $id_marque . $id_categorie . $date . $date . ")");
                }
                elseif(($produit_existe == 1) && ($data[7] == 0))
                {
                    $id_product = Db::getInstance()->getValue('
                            SELECT `id`
                            FROM `'._DB_PREFIX_.'product`
                            WHERE `reference` = '.$reference);
                    Db::getInstance()->execute("UPDATE `"._DB_PREFIX_."product` SET reference = ". $reference . ", name =". $titre . ", description_short =". $recapitulatif . ", description =". $description . ", id_manufacturer =". $id_marque . ", id_category_default =" . $id_categorie . " WHERE id =". $id_product );
                }

                elseif(($produit_existe == 0) && ($data[7] == 1))
                {
                    Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."product` ('name', 'description_short', 'description', 'id_manufacturer', 'id_category_default', 'date_add', 'date_upd') VALUES(". $titre. $recapitulatif . $description . $id_marque . $id_categorie . $date . $date . ")");
                    $id_product = Db::getInstance()->getValue('
                            SELECT `id`
                            FROM `'._DB_PREFIX_.'product`
                            WHERE `name` = '.$titre . 'AND `description_short` = ' . $recapitulatif . 'AND `description` = ' . $description . 'AND `id_manufacturer` = ' . $id_marque . 'AND `id_category_default` = ' . $id_categorie);
                }

                elseif(($produit_existe == 1) && ($data[7] == 1))
                {
                    $id_product = Db::getInstance()->getValue('
                            SELECT `id_product`
                            FROM `'._DB_PREFIX_.'product_attribute`
                            WHERE `reference` = '.$reference);
                    Db::getInstance()->execute("UPDATE `"._DB_PREFIX_."product` SET reference = ". $reference . ", name =". $titre . ", description_short =". $recapitulatif . ", description =". $description . ", id_manufacturer =". $id_marque . ", id_category_default =" . $id_categorie . " WHERE id =". $id_product );
                }






            }
            elseif($data[0] == "information_declinaison")
            {
                $reference = $data[1];
                $groupe_declinaison[0] = $data[2];
                $type_declinaison[0] = $data[3];
                $groupe_declinaison[1] = $data[4];
                $type_declinaison[1] = $data[5];
                $groupe_declinaison[2] = $data[6];
                $type_declinaison[2] = $data[7];
                $groupe_declinaison[3] = $data[8];
                $type_declinaison[3] = $data[9];

                if( (!(empty($groupe_declinaison[0]))) && (!(empty($type_declinaison[0]))) )
                {
                    $id_groupe_declinaison[0] = Db::getInstance()->getValue('
                            SELECT `id_attribute_group`
                            FROM `'._DB_PREFIX_.'attribute_group_lang`
                            WHERE `name` = '.$groupe_declinaison[0]);
                    $id_type_declinaison[0] = Db::getInstance()->getValue('
                            SELECT `id_attribute`
                            FROM `'._DB_PREFIX_.'attribute_lang`
                            WHERE `name` = '.$type_declinaison[0]);
                    if( (!(empty($groupe_declinaison[1]))) && (!(empty($type_declinaison[1]))) )
                    {
                        $id_groupe_declinaison[1] = Db::getInstance()->getValue('
                            SELECT `id_attribute_group`
                            FROM `'._DB_PREFIX_.'attribute_group_lang`
                            WHERE `name` = '.$groupe_declinaison[1]);
                        $id_type_declinaison[1] = Db::getInstance()->getValue('
                            SELECT `id_attribute`
                            FROM `'._DB_PREFIX_.'attribute_lang`
                            WHERE `name` = '.$type_declinaison[1]);

                        if( (!(empty($groupe_declinaison[2]))) && (!(empty($type_declinaison[2]))) )
                        {
                            $id_groupe_declinaison[2] = Db::getInstance()->getValue('
                            SELECT `id_attribute_group`
                            FROM `'._DB_PREFIX_.'attribute_group_lang`
                            WHERE `name` = '.$groupe_declinaison[2]);
                            $id_type_declinaison[2] =  Db::getInstance()->getValue('
                            SELECT `id_attribute`
                            FROM `'._DB_PREFIX_.'attribute_lang`
                            WHERE `name` = '.$type_declinaison[2]);
                        }
                    }
                }

                $sql = 'SELECT COUNT(`product_attribute`) FROM '._DB_PREFIX_.'product_attribute WHERE `reference` = ' . $reference;
                if( Db::getInstance()->getValue($sql) == 0)
                    $declinaison_existe = 0;
                elseif ( Db::getInstance()->getValue($sql) == 1)
                    $declinaison_existe = 1;
                else
                    echo "Un bug s'est produit : Voir ligne ".$i;

                if($declinaison_existe == 1)
                {
                    $id_product_attribute = Db::getInstance()->getValue('
                        SELECT `id_product_attribute`
                        FROM `'._DB_PREFIX_.'product_attribute`
                        WHERE `reference` = '.$reference);
                }
                elseif($declinaison_existe == 0)
                {
                    Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."product_attribute` ('id_product', 'reference') VALUES(" . $id_product . $reference . ")");
                    $id_product_attribute = Db::getInstance()->getValue('
                        SELECT `id_product_attribute`
                        FROM `'._DB_PREFIX_.'product_attribute`
                        WHERE `reference` = '.$reference);
                }







            }
            else
            {
                echo "Un bug s'est produit : Voir ligne ".$i;
            }

            $i++;

        }
        fclose($fichier);

        return $output;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitImportCSVModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'IMPORTCSV_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'IMPORTCSV_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'IMPORTCSV_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'IMPORTCSV_LIVE_MODE' => Configuration::get('IMPORTCSV_LIVE_MODE', true),
            'IMPORTCSV_ACCOUNT_EMAIL' => Configuration::get('IMPORTCSV_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'IMPORTCSV_ACCOUNT_PASSWORD' => Configuration::get('IMPORTCSV_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }
}
